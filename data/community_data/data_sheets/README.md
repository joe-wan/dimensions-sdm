Files from Kabir, converted to CSV:
- `master.*.csv`: different tabs from Kabir's master spreadsheet, `master_data_sheet.xlsx`
- `core_colors.20150729.csv`: colors for mapping to the North American continent
- `sites.20150731.csv`: latest site data
- `joe.id_corrections.csv`: my manual mapping from the Sample.ID.ILL column to the actual IDs in the OTU table (`otus_454ILL_table_wTax.biom`)
- `Soil.NorthAmerica.Fungi.Peay.biom.metadata.csv`: sample metadata extracted from the BIOM file Kabir gave Josh (`Soil.NorthAmerica.Fungi.Peay.biom.metadata.csv`)
