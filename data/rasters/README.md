# Rasters

This directory contains raster data. Currently, I'm just using Josh's data but eventually I'll need to figure out how he's pre-processing them (is he just using the Pinaceae range mask to remove data outside the range?).

## 2016-08-19_processed_ladau

Josh sent me these 10 climate rasters. The original data is from the Climate Research Unit (e.g. https://crudata.uea.ac.uk/cru/data/hrg/).
