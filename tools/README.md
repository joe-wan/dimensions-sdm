# Tools
This directory contains external tools used for analysis.

## Contents

- `SpeciesDistributionModeling/`: Josh Ladau's SDM tool. To install, type
  `git clone https://github.com/jladau/SpeciesDistributionModeling.git` from
  the tools directory.
