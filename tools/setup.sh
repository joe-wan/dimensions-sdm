#!/usr/bin/env bash

# Make sure we're in the tools/ directory
REPO=$(git rev-parse --show-toplevel)
cd $REPO/tools

# Make directory to store dowloaded archives
mkdir -p archives/

# Download Josh's SDM code from GitHub

# Original version
if [ ! -e "SpeciesDistributionModeling-2016-08-16/" ]; then
  curl -o archives/SpeciesDistributionModeling-2016-08-16.zip -O -J -L https://github.com/jladau/SpeciesDistributionModeling/archive/fefb8806da2118f1b55593efb18f998be1a0596a.zip
  unzip archives/SpeciesDistributionModeling-2016-08-16.zip -d archives/
  mv archives/SpeciesDistributionModeling-fefb8806da2118f1b55593efb18f998be1a0596a SpeciesDistributionModeling-2016-08-16/
fi

# Most recent version as of 2017-02-02
if [ ! -e "SpeciesDistributionModeling-2017-02-03c/" ]; then
  curl -o archives/SpeciesDistributionModeling-2017-02-03c.zip -O -J -L https://github.com/jladau/SpeciesDistributionModeling/archive/0cfcbbbb1ee517072483c10e82d30bae6214c2a2.zip
  unzip archives/SpeciesDistributionModeling-2017-02-03c.zip -d archives/
  mv archives/SpeciesDistributionModeling-0cfcbbbb1ee517072483c10e82d30bae6214c2a2 SpeciesDistributionModeling-2017-02-03c/
fi

# Download GDAL
if [ ! -e "gdal-2.1.3/"]; then
  curl -o archives/gdal-2.1.3.tar.gz http://download.osgeo.org/gdal/2.1.3/gdal-2.1.3.tar.gz
  tar -xf archives/gdal-2.1.3.tar.gz
fi

(cd gdal-2.1.3/
  ./configure
  make -j 4)
