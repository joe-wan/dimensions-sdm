# September 2016

## 2016-09-08: Meeting with Josh Ladau

Met with Josh starting 2:00 PM today.

Clarified various issues with running the SDM software:

- **Loading rasters.**
  We need to specify the `--sTrainingDate` parameter. For us this does not
  matter (pick arbitrarily)--the rasters only have one set of values--but does
  matter if we're trying to use historical data, etc. Josh says he fixed this
  for the next version of the software.
- **Normalizing OTU data.**
  The software was failing on my BIOM files because they had raw counts. To fix
  this, we have to specify `--bNormalize=true` to normalize to the range $0.0 -
  1.0$ (otherwise the logit tranformation is not defined!)
- **Subsetting the data.**
  OH and AH will have to be separate files.

Conceptual clarifications:

- **SDM and dispersal limitation.**
  Josh is not sold on using SDM to infer dispersal limitation--since there's
  always some chance that the apparent limitation is really due to some niche
  axis not included in the model. However, an approach where e.g. we model west
  of the Rockies and project the resulting SDM onto the East coast might be
  feasible but I think we'll have to look at the data and discuss.
- **Relative importance.**
  Taking the selected models and looking at the relative importance of different
  predictors might be useful (e.g. the R package `relimp`). This seems to be
  pretty standard, and could be used to compare the factors which determine
  niches of e.g. EcM vs. non-EcM fungi.
- **Model validation.**
  Asked Josh whether we can use presence data to validate our models. If we had
  presence-absence, we could take the predicted abundance and see how that
  corresponds to presence/absence (e.g. logistic regression); Josh said this was
  reasonable. But since most data is actually presence-only, it's trickier.
  Maybe we can generate pseudo- absences like people do with MaxEnt?

### To-do list:

- Add `s__` prefixes to taxonomy strings
- Split AH and OH data
- MESS and masking issue: try a couple of other things
- Get test data to Josh to replicate MESS mask issue (Dropbox)
- Set Skype meeting with Kabir. (Josh: best Thursday after 2:30 PM or Friday, but
  other days possible)

### Preliminary models

These models aren't good because AH + OH data should not be used together.
Nonetheless, they're interesting to look at:

> ![OTU_463: Amanita rubescens](graphics/2016-09-08_OTU_463.svg)
> **OTU_463: Amanita rubescens** This OTU is really A. novinupta, I think, and
> is only found west of the Rockies. The model is predicting very high
> abundances in the Pacific Northwest and Canada's west coast. Wonder how this
> matches up with our actual data.

Table:

| id        | species                                      |
|:----------|:---------------------------------------------|
| OTU_1     | Mortierella humilis Mortierella verticillata |
| OTU_31    | unclassified_Phialocephala                   |
| OTU_59    | unclassified_Leucosporidiaceae               |
| OTU_6     | Cryptococcus terricola                       |
| OTU_5     | uncultured Cenococcum                        |
| OTU_7     | unclassified_Mortierella                     |
| OTU_2     | Wilcoxina rehmii                             |
| OTU_3     | unclassified_Piloderma                       |
| OTU_14742 | unclassified_Cortinarius                     |
| OTU_20    | unclassified_Piloderma                       |
| OTU_556   | unclassified_Lactarius                       |
| OTU_580   | unclassified_Suillus                         |
| OTU_18613 | Suillus brevipes                             |
| OTU_459   | unclassified_Suillus                         |
| OTU_145   | unclassified_Lactarius                       |
| OTU_17019 | Suillus tomentosus                           |
| OTU_463   | Amanita rubescens                            |
| OTU_209   | unclassified_Lactarius                       |
| OTU_2144  | unclassified_Amanita                         |
| OTU_23    | Lactarius rufus                              |
| OTU_579   | unclassified_Amanita                         |
| OTU_470   | Lactarius musteus                            |
| OTU_125   | Amanita flavoconia                           |
| OTU_1746  | unclassified_Suillus                         |
| OTU_3621  | Amanita porphyria                            |

| id        | max OH | max AH | # OH | # AH | avg OH | avg AH | lat rng | lon rng | # E | # W |
|:----------|:-------|:-------|:-----|:-----|:-------|:-------|:--------|:--------|:----|:----|
| OTU_1     | 28.32  | 29.8   | 60   | 65   | 4.267  | 2.954  | 35      | 75      | 74  | 51  |
| OTU_31    | 1.84   | 3.56   | 50   | 57   | 0.427  | 0.649  | 34      | 69      | 68  | 39  |
| OTU_6     | 6.72   | 6.64   | 53   | 49   | 0.615  | 0.569  | 35      | 75      | 62  | 40  |
| OTU_59    | 2.12   | 4.68   | 47   | 53   | 0.323  | 0.560  | 35      | 75      | 64  | 36  |
| OTU_5     | 8.52   | 7.08   | 46   | 46   | 0.966  | 0.783  | 35      | 75      | 65  | 27  |
| OTU_7     | 7.64   | 9.32   | 44   | 44   | 0.743  | 0.777  | 35      | 75      | 52  | 36  |
| OTU_2     | 15.24  | 8      | 33   | 43   | 1.762  | 1.089  | 35      | 75      | 63  | 13  |
| OTU_3     | 21.8   | 11.96  | 31   | 35   | 2.199  | 1.238  | 35      | 75      | 42  | 24  |
| OTU_20    | 21.72  | 7.84   | 27   | 27   | 1.097  | 0.558  | 34      | 75      | 44  | 10  |
| OTU_14742 | 2.84   | 2.76   | 22   | 29   | 0.199  | 0.324  | 24      | 37      | 41  | 10  |
| OTU_556   | 0.6    | 1.24   | 13   | 23   | 0.028  | 0.081  | 20      | 44      | 28  | 8   |
| OTU_459   | 0.64   | 0.88   | 7    | 13   | 0.018  | 0.052  | 24      | 27      | 17  | 3   |
| OTU_145   | 5.52   | 3.56   | 9    | 10   | 0.247  | 0.126  | 10      | 33      | 1   | 18  |
| OTU_580   | 0.16   | 2.04   | 3    | 14   | 0.004  | 0.067  | 25      | 38      | 13  | 4   |
| OTU_18613 | 0.32   | 2.08   | 3    | 14   | 0.010  | 0.067  | 10      | 16      | 17  | 0   |
| OTU_463   | 7.48   | 10.76  | 8    | 7    | 0.331  | 0.340  | 17      | 21      | 0   | 15  |
| OTU_17019 | 0.44   | 0.16   | 6    | 8    | 0.012  | 0.009  | 24      | 26      | 10  | 4   |
| OTU_209   | 6.48   | 0.84   | 6    | 7    | 0.179  | 0.040  | 12      | 32      | 1   | 12  |
| OTU_23    | 14.36  | 5.2    | 6    | 6    | 0.526  | 0.218  | 4       | 37      | 5   | 7   |
| OTU_470   | 0.28   | 1.2    | 6    | 5    | 0.020  | 0.042  | 8       | 36      | 4   | 7   |
| OTU_125   | 5.8    | 2.64   | 4    | 5    | 0.126  | 0.105  | 9       | 13      | 0   | 9   |
| OTU_1746  | 0.16   | 0.2    | 4    | 5    | 0.007  | 0.006  | 9       | 13      | 0   | 9   |
| OTU_3621  | 0.12   | 0.4    | 5    | 3    | 0.006  | 0.011  | 6       | 15      | 0   | 8   |
| OTU_2144  | 0.08   | 0.44   | 1    | 7    | 0.001  | 0.017  | 16      | 49      | 7   | 1   |
| OTU_579   | 0.12   | 1.8    | 2    | 6    | 0.002  | 0.042  | 10      | 25      | 7   | 1   |

![](graphics/2016-08-23_lonrange_vs_latrange.svg)
![](graphics/2016-08-23_prop_west_vs_occurences.svg)
![](graphics/2016-08-23_west_vs_east.svg)
