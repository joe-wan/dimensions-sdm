# 2016-09-01 Model Test

This is a basic test to see if the SDM pipeline works on our data.

## Contents

### `params/`: Parameters for the analysis
- `params/selected_otus_25.txt`: The same as `$GIT_ROOT/analysis/2016-08-23_clean_community_data/selections/selected_otus_25.txt`, but I'm copying it here since it's a small and important file.
- `params/raster_list.csv`: These indicate paths to rasters for
