#!/usr/bin/env bash

######################## Set variables for useful paths ########################

# Set some useful absolute paths as environment variables
REPO=$(git rev-parse --show-toplevel)
ANALYSIS=$REPO/analysis/
WDIR=$(pwd)

######################### Locations of tools and inputs ########################

# Locations of tools
SDM_DIR=$REPO/tools/SpeciesDistributionModeling-2016-08-16
SDM=$SDM_DIR/bin/SpeciesDistributionModeling.jar

# Locations of inputs
IN_BIOM=$ANALYSIS/2016-08-23_clean_community_data/biom_out/dimensions_jw_pooled.basic.biom
IN_OTUS=$ANALYSIS/2016-08-23_clean_community_data/selections/selected_otus_25.csv
MASK=$REPO/data/masks/pinaceae_dissolve.shp

################################ Generate models ###############################

# 1. Prepare input lists
# ----------------------

# We need to convert the rasters in the raster list to absolute paths. Rather
# than having to change the list on every platform, I'll expand $REPO to
# whatever the root of the repository is and place output in a temporary file.
>params/raster_list.csv.tmp
while read p; do
  echo ${p/'$REPO'/$REPO} >> params/raster_list.csv.tmp
done <params/raster_list.csv

# The response list I'm dynamically generating to use OTUs previously selected
cp params/response_list.csv params/response_list.csv.tmp
cat $IN_OTUS >> params/response_list.csv.tmp


# 2. Run model selection
# ----------------------

java -cp "$SDM" edu.ucsf.ModelSelector.ModelSelectorLauncher \
  --sBIOMPath="$IN_BIOM" \
  --sRasterListPath="$WDIR/params/raster_list.csv.tmp" \
  --sResponseVarsListPath="$WDIR/params/response_list.csv.tmp" \
  --sOutputPath="$WDIR/sdm_out/model_selection_out.csv" \
  --bPrintData=true \
  --iRarefactionTotal=2500 \
  --bNormalize=true \
  --sTrainingDate=2011-01-01
# Rarefying to 2500 seems reasonable.

# 3. Project models and run MESS
# ------------------------------

# Make output directories
mkdir -p $WDIR/sdm_out/projections
# Track which line we're processing
INPUTLINE=0
# Loop through results and process
while read LINE; do
  # Only process if not the header line
  if [ "$INPUTLINE" -ne "0" ]; then
    # Get OTU name (this is field 7 in the output)
    OTU=$(echo "$LINE" | cut -f7 --delim=,)
    echo "Projecting and running MESS for: $OTU"
    # Model projection
    java -cp "$SDM" edu.ucsf.ModelProjector.ModelProjectorLauncher \
      --sBIOMPath="$IN_BIOM" \
      --sOutputPath="$WDIR/sdm_out/projections/$OTU.nc" \
      --sSelectedModelsPath="$WDIR/sdm_out/model_selection_out.csv" \
      --iRarefactionTotal=2500 \
      --bNormalize=true \
      --sMaskPath="$MASK" \
      --sMaskFeature=CODE \
      --sMaskFeatureID=1 \
      --rgtProjectionTimes=2011-01-01 \
      --iInputLine=$INPUTLINE
    # MESS
    java -cp "$SDM" edu.ucsf.MESS.MESSLauncher \
      --sBIOMPath="$IN_BIOM" \
      --sOutputPath="$WDIR/sdm_out/projections/$OTU.mess.nc" \
      --sSelectedModelsPath="$WDIR/sdm_out/model_selection_out.csv" \
      --iRarefactionTotal=2500 \
      --bNormalize=true \
      --bOutputTable=true \
      --rgtProjectionTimes=2011-01-01 \
      --iInputLine=$INPUTLINE
    # Note: currently MESS has a bug preventing us from using the mask
  fi
  # Increment input line
  INPUTLINE=$((INPUTLINE + 1))
done <$WDIR/sdm_out/model_selection_out.csv
