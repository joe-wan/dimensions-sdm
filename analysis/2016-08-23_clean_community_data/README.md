Scripts and raw data for preparing BIOM files with OTU and sample metadata.

## Instructions:

To create BIOM files from Kabir's phyloseq data, run:
`Rscript process_phyloseq.R`

Then, to print the list of species of interest, run:
`Rscript select_species.R`


## Scripts

- `process_old_biom.R`: Half-completed script to process the BIOM file from
Kabir's Dimensions directory (`raw_data/otus_454ILL_table_wTax.biom`)
- `process_phyloseq.R`: Script to create BIOM files from Kabir's newest phyloseq
object (`raw_data/psDIM.clean.RData`)


## Outputs

### `dimensions_jw.biom`
OTU table exactly as Kabir has it in his phyloseq object. I took a subset of and renamed the sample metadata fields:
- `sample` sample name (corrected to origninally-intended naming)
- `old.id` original ID from NGS bioinformatics pipeline
- `latitude` latitude of individual sample (decimal degrees)
- `longitude` longitude of individual sample  (decimal degrees)
- `origin.latitude` latitude of site's "origin" sample (decimal degrees)
- `origin.longitude` longitude of site's "origin" sample  (decimal degrees)
- `site` site code
- `transect` ID of transect (A, B, C, etc.)
- `distance.label` distance label along transect
- `distance.transect` actual distance along transect (m)
- `horizon` horizon (`OH` or `AH`)
- `datetime` date of sampling
- `experiment` indicates a set of samples; `Dimensions.Core` are the "real" DoB samples
- `sample.type` type of sample; `Environmental` is for DoB soil samples
- `elevation.m` elevation of site
- `primary.host` dominant Pinaceae host species at a site
- `non.em.ba.m2` basal area of non-EcM plants (m<sup>2</sup>)
- `ecm.ba.m2` basal area of EcM plants (m<sup>2</sup>)
- `non.pinaceae.em.ba.m2` basal area of EcM plants, excluding Pinaceae (m<sup>2</sup>)
- `pinaceae.ba.m2` basal area of Pinaceae EcM plants (m<sup>2</sup>)
- `total.plant.richness` total richness of plants at site
- `em.plant.richness` richness of EcM plants at site
- `platform` sequencing platform (`Roche_454` or `Illumina`)
- `pool.id` ID indicating site, horizon, and platform; used to pool pseudoreplicate samples

Taxonomy info is kept as it was in Kabir's phyloseq object:
- `domain, phylum, class, order, family, genus, species`: 7 taxonomic levels, as separate metadata fields
- `taxonomy`: taxonomy as a list (standard for BIOM taxonomy)
- `RDP.taxon`: taxonomy as from Kabir's RDP classifier run
- `...`: remaining fields reflect FunGuild assignments

### `dimensions_jw_rarefied.biom`
Rarefied version of above: rarefied in phyloseq to 500 sequences per sample; low-depth samples and 0-count OTUs dropped. Metadata as above.

### `dimensions_jw_pooled.biom`
Using the rarefied data from `dimensions_jw_rarefied.biom`, I pooled samples from the same horizon at the same site, sequenced on the same platform. New IDs are assigned (from `pool.id` in previous BIOM files) reflecting site, horizon, and platform. Taxonomy metadata is the same, but sample metadata only includes fields reflecting per-site properties. Changed/new fields:
- `latitude, longitude`: now indicate the coordinates for the site's origin point
- `samples`: number of samples pooled for this site
