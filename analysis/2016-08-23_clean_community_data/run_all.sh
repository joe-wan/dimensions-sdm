#!/usr/bin/env bash

mkdir -p biom_out/ R_out/ logs/ selections/ selections/plots/
touch biom_out/.keep R_out/.keep selections/.keep selections/plots/.keep
# Selections is an output but I want to version it since it'll
# contain small and useful outputs (hence no _out/ suffix)

# Process and select species
Rscript process_phyloseq.R 2>&1 | tee logs/process_phyloseq.log
Rscript select_species.R 2>&1 | tee logs/process_phyloseq.log
