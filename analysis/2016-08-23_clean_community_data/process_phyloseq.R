library(biomformat)
library(phyloseq)
library(dplyr)


##### Load and fix Kabir's new phyloseq object #####

# Load and extract data
load('../../data/community_data/raw_data/psDIM.clean.RData')
sample.data <- sample_data(psDIM.clean)
otu.table <- otu_table(psDIM.clean)

# Sample IDs and Platform column disagree for some samples
sample.data$Platform[endsWith(sample.data$Sample.ID.1, '.ILL')] <- "Illumina"
sample.data$Platform[endsWith(sample.data$Sample.ID.1, '.454')] <- "Roche_454"

# Append .ILL and .454 to the OR4.C10.OH sample IDs
bad.ids <- c('OR4.C10.OH.ILL', 'OR4.C10.OH.454')
sample.data$Sample.ID.FIELD[match(bad.ids, sample.data$Sample.ID.1)] <- bad.ids

# Add row names as an old ID column
sample.data$old.id <- rownames(sample.data)


##### Get site info from old BIOM #####

# Read data
old.sample <- read.csv('../../data/community_data/data_sheets/Soil.NorthAmerica.Fungi.Peay.biom.metadata.csv',
                       stringsAsFactors = FALSE)

# Get site (first field delimited by '.' in sample name)
old.sample <- old.sample %>% rowwise() %>%
  mutate(site=strsplit(sample, '.', fixed=TRUE)[[1]][1]) %>%
  mutate(site=ifelse( # ALB[1-4] sites should be AB[1-4]
    startsWith(site, 'ALB'),
    paste('AB', substr(site, 4, 4), sep=''),
    site))

# Group by site and take an arbitrary record's sampling data
site.info <- old.sample %>% ungroup() %>% group_by(site) %>%
  slice(1) %>% select(c(site, datetime))


##### Load extra site data #####

# Load site data
new.site.data <- read.csv('../../data/community_data/data_sheets/sites.20150731.csv', stringsAsFactors=FALSE)

# This sheet has additional information but 'AK1' and 'AK2' are used instead of 'AL1' and 'AL2'
new.site.data$Site.ID[match(c('AK1', 'AK2'), new.site.data$Site.ID)] = c('AL1', 'AL2')


##### Prepare final sample and OTU tables #####

# Previous these were the fields I was keeping--but I reduced the number later
# fields = c(
#   "Sample.ID.FIELD", "Sample.ID.1", "Experiment", "Sample.Type", "Platform",
#   "Site", "Transect", "Distance.Label", "Distance.Transect", "Horizon", "Notes", "Azimuth_Degress",
#   "dec.deg.x.orig", "dec.deg.y.orig", "new.long.dd", "new.lat.dd", "longitude.dd", "latitude.dd",
#   "Dom.Plant",
#   "Perc.Soil.Moisture", "Perc.N", "Perc.C", "CNRatio", "ng.NO3.N.per.g.soil", "ng.NH4.N.per.g.soil", "pH",
#   # "Soil.CBH", "Soil.BG", "Soil.AG", "Soil.NAG", "Soil.BX", "Soil.AP", "Soil.BGLU", "Soil.LAP", "Soil.PPO", "Soil.PER",
#   # "Tip.CBH", "Tip.BG", "Tip.AG", "Tip..G", "Tip.BX", "Tip.AP", "Tip.BGLU", "Tip.LAP", "Tip.PPO", "Tip.PER",
#   "core.basal.area", "core.NM.basal.area", "core.EM.basal.area")

# Join the tables and select the data we'd like to keep
final.sample <- left_join(sample.data, site.info, by=c('Site'='site')) %>%
  left_join(new.site.data, by=c('Site'='Site.ID')) %>%
  transmute(sample=Sample.ID.FIELD, latitude=new.lat.dd, longitude=new.long.dd, datetime=datetime, old.id=old.id,
    origin.latitude=Lat.dd, origin.longitude=Long.dd,
    site=Site, transect=Transect, distance.label=Distance.Label, distance.transect=Distance.Transect, horizon=Horizon,
    experiment=Experiment, sample.type=Sample.Type, platform=Platform,
    primary.host=Primary.Host, elevation.m=Elevation.m,
    total.plant.richness=Total.Plant.Richness, em.plant.richness=EM.Plant.Richness,
    non.em.ba.m2=NonECM.BA.m2, ecm.ba.m2=ECM.BA.m2, pinaceae.ba.m2=Pinaceae.BA.m2, non.pinaceae.em.ba.m2=non.Pine.EM.BA) %>%
  mutate(pool.id=paste(site, horizon, ifelse(platform == 'Roche_454', '454', 'ILL'), sep='.'))

# Prepare final BIOM data
# Rename samples in OTU table
final.otu <- otu.table
colnames(final.otu) <- final.sample$sample

# Rename rows in sample table
rownames(final.sample) <- final.sample$sample

# Get taxonomy table
final.tax <- mutate(
  data.frame(tax_table(psDIM.clean)),
  taxonomy=paste(domain, phylum, class, order, family, genus, species, sep=';')
)
rownames(final.tax) <- rownames(tax_table(psDIM.clean))


##### Export a BIOM file #####

# Helper functions: extract names of numeric and integer columns
numeric.columns <- function(df) {
  classes <- sapply(df, class)
  names <- colnames(df)
  return(names[classes == 'numeric'])
}
integer.columns <- function(df) {
  classes <- sapply(df, class)
  names <- colnames(df)
  return(names[classes == 'integer'])
}

# Function for exporting to biom. Uses the `biom` command line tool.
# Weird filenames (spaces, etc.) are not escaped.
# If use.data.types is set to FALSE, all data fields will be treated as strings.
export_biom <- function(otu.table, path, sample.table=NULL, tax.table=NULL, keep.files=FALSE, use.data.types=TRUE) {
  # Dump a TSV file
  tsv.path <- paste(path, '.tsv.tmp', sep='')
  basic.biom.path <- paste(path, '.tmp', sep='')
  cat('#OTU ID\t', file=tsv.path, sep='')
  write.table(otu.table, tsv.path,
              sep='\t', quote=FALSE, append=TRUE, col.names=TRUE, row.names=TRUE)

  # Make a bare-bones BIOM file (no metadata)
  system(paste('biom convert -i', tsv.path, '-o', basic.biom.path, '--table-type="OTU table" --to-hdf5'))

  # Now build up commands for adding metadata
  command <- ''
  int.fields <- c()
  float.fields <- c()
  remove.command <- paste('rm', tsv.path)

  # Dump a sample metadata file
  if (!is.null(sample.table)) {
    sample.path <- paste(path, '.sample.tmp', sep='')
    cat('#Sample ID\t', file=sample.path, sep='')
    write.table(sample.table, sample.path,
                sep='\t', quote=FALSE, append=TRUE, col.names=TRUE, row.names=TRUE,
                fileEncoding='UTF-8')
    command <- paste(command, '--sample-metadata-fp', sample.path)
    int.fields <- c(int.fields, integer.columns(sample.table))
    float.fields <- c(float.fields, numeric.columns(sample.table))
    remove.command <- paste(remove.command, sample.path)
  }

  # Dump a taxonomy metadata file
  if (!is.null(tax.table)) {
    tax.path <- paste(path, '.tax.tmp', sep='')
    cat('#OTU ID\t', file=tax.path, sep='')
    write.table(tax.table, tax.path,
                sep='\t', quote=FALSE, append=TRUE, col.names=TRUE, row.names=TRUE,
                fileEncoding='UTF-8')
    command <- paste(command, '--observation-metadata-fp', tax.path, '--sc-separated', 'taxonomy')
    int.fields <- c(int.fields, integer.columns(tax.table))
    float.fields <- c(float.fields, numeric.columns(tax.table))
    remove.command <- paste(remove.command, tax.path)
  }

  if (is.null(tax.table) && is.null(tax.table)) {
    system(paste('mv', basic.biom.path, path))
  } else {
    fields <- ''
    if (use.data.types && length(float.fields) > 0) {
      fields <- paste('--float-fields', paste(float.fields, collapse=','))
    }
    if (use.data.types && length(int.fields) > 0) {
      fields <- paste('--int-fields', paste(int.fields, collapse=','))
    }
    command <- paste('biom add-metadata -i', basic.biom.path, '-o', path, command,
                     fields)
    remove.command <- paste(remove.command, basic.biom.path)
    print(command)
    system(command)
  }

  # Remove temporary outputs, if required
  if (!keep.files) {
    system(remove.command)
  }
}

# Export pre-rarefaction BIOM with sample data
export_biom(final.otu, 'biom_out/dimensions_jw.biom',
            sample.table=final.sample,
            tax.table=final.tax)
export_biom(final.otu, 'biom_out/dimensions_jw.basic.biom',
            sample.table=select(final.sample, latitude, longitude, datetime),
            tax.table=select(final.tax, taxonomy), use.data.types=FALSE)

##### Export rarefied and pooled data #####

# Rarefy the OTU table
rarefied.otu <- rarefy_even_depth(final.otu, sample.size=500, rngseed=20160822)
rarefied.sample <- final.sample[colnames(rarefied.otu),]
rarefied.tax <- final.tax[rownames(rarefied.otu),]

# Export rarefied BIOM
export_biom(rarefied.otu, 'biom_out/dimensions_jw_rarefied.biom',
            sample.table=rarefied.sample,
            tax.table=rarefied.tax)
export_biom(rarefied.otu, 'biom_out/dimensions_jw_rarefied.basic.biom',
            sample.table=select(rarefied.sample, latitude, longitude, datetime),
            tax.table=select(rarefied.tax, taxonomy), use.data.types=FALSE)

# Pool samples by site/horizon
pooled.otu <- t(merge_samples(rarefied.otu, rarefied.sample$pool.id)) # For some reason this transposes the data...

# Find columns that have only one value across the samples in a pool
values.per.pool <- sapply(
  rarefied.sample %>% group_by(pool.id) %>% summarise_each(funs(n_distinct)),
  max)
pool.columns <- names(values.per.pool)[values.per.pool == 1]

# Take good fields and create a new sample table
samples.grouped <- rarefied.sample %>%
  select(pool.id, one_of(pool.columns)) %>%
  group_by(pool.id)
pooled.sample <- samples.grouped %>%
  slice(1) %>%
  left_join(summarize(samples.grouped, samples=n())) %>%
  rename(latitude=origin.latitude, longitude=origin.longitude)
pooled.sample <- data.frame(pooled.sample)
rownames(pooled.sample) <- pooled.sample$pool.id
pooled.sample <- select(pooled.sample, -pool.id)

# Create final tables and export
pooled.sample <- pooled.sample[colnames(pooled.otu),]
pooled.tax <- final.tax[rownames(pooled.otu),]
export_biom(pooled.otu, 'biom_out/dimensions_jw_pooled.biom',
            sample.table=pooled.sample,
            tax.table=pooled.tax)
export_biom(pooled.otu, 'biom_out/dimensions_jw_pooled.basic.biom',
            sample.table=select(pooled.sample, latitude, longitude, datetime),
            tax.table=select(pooled.tax, taxonomy), use.data.types=FALSE)
export_biom(pooled.otu, 'biom_out/dimensions_jw_pooled.basic.typed.biom',
            sample.table=select(pooled.sample, latitude, longitude, datetime),
            tax.table=select(pooled.tax, taxonomy), use.data.types=TRUE)

# Export all the data as .RData
save(final.otu, final.sample, final.tax,
     rarefied.otu, rarefied.sample, rarefied.tax,
     pooled.otu, pooled.sample, pooled.tax,
     file='R_out/dimensions_jw_raw.RData')

# Make phyloseq objects and export as .RData
my_tax_table <- function(tax) {
  # Preserves row names
  result <- tax_table(tax)
  taxa_names(result) <- rownames(tax)
  return(result)
}
final <- phyloseq(otu_table(final.otu), sample_data(final.sample), my_tax_table(final.tax))
rarefied <- phyloseq(otu_table(rarefied.otu), sample_data(rarefied.sample), my_tax_table(rarefied.tax))
pooled <- phyloseq(otu_table(pooled.otu), sample_data(pooled.sample), my_tax_table(pooled.tax))
save(final, rarefied, pooled, file='R_out/dimensions_jw_phyloseq.RData')
