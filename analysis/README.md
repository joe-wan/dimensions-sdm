# Dimensions of Fungal Biodiversity, Species Distribution Modeling

This project aims to build species distribution models (SDMs) of fungi in Pinaceae-dominated forest soils across North America.
