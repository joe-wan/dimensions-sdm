#!/usr/bin/env bash

mkdir -p biom_out/ logs/
touch biom_out/.keep

# Make cleaned-up BIOM files.
Rscript process_phyloseq.R 2>&1 | tee logs/process_phyloseq.log

# Zip the BIOM files.
for i in biom_out/*.biom; do gzip -k $i; done
